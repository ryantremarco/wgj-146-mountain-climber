extends CanvasLayer

func set_score(score):
	$Score/Label.text = str(score) + "m";
	$EndGamePanel/NinePatchRect/MarginContainer/HBoxContainer/Score.text = str(score) + "m";

func game_over():
	$Score.visible = false;
	$EndGamePanel.visible = true;

func update_prompts(controller_type):
	var options = $EndGamePanel/NinePatchRect/MarginContainer/HBoxContainer/Options;
	if controller_type == "KEYBOARD":
		options.text = "[R] restart\n[Esc] menu";
	elif controller_type == "GAMEPAD":
		options.text = "[Tri/Y] restart\n[Optn/Start] menu";
