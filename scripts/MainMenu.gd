extends Control

var controller_type = "KEYBOARD";

func _ready():
	if OS.get_name() == "HTML5":
		$Buttons/NinePatchRect/MarginContainer/MainButtons/QuitButton.visible = false;

func _on_PlayButton_pressed():
	get_tree().change_scene("res://scenes/Game.tscn");


func _on_QuitButton_pressed():
	get_tree().quit();


func _on_BackButton_pressed():	
	$Buttons/NinePatchRect/MarginContainer/MainButtons.visible = true;
	$Buttons/NinePatchRect/MarginContainer/Settings.visible = false;
	$Buttons/NinePatchRect/MarginContainer/Credits.visible = false;
	if controller_type == "GAMEPAD":
		$Buttons/NinePatchRect/MarginContainer/MainButtons/PlayButton.grab_focus();


func _on_SettingsButton_pressed():
	$Buttons/NinePatchRect/MarginContainer/MainButtons.visible = false;
	$Buttons/NinePatchRect/MarginContainer/Settings.visible = true;
	if controller_type == "GAMEPAD":
		$Buttons/NinePatchRect/MarginContainer/Settings/BackButton.grab_focus();


func _on_MasterSlider_value_changed(value):
	change_bus_volume(AudioServer.get_bus_index("Master"), value);


func _on_MusicSlider_value_changed(value):
	change_bus_volume(AudioServer.get_bus_index("Music"), value);


func _on_SFXSlider_value_changed(value):
	change_bus_volume(AudioServer.get_bus_index("SFX"), value);

func change_bus_volume(bus_index, value):
	AudioServer.set_bus_mute(bus_index, value==-20);
	AudioServer.set_bus_volume_db(bus_index, value);

func _on_CreditsButton_pressed():
	$Buttons/NinePatchRect/MarginContainer/MainButtons.visible = false;
	$Buttons/NinePatchRect/MarginContainer/Credits.visible = true;
	if controller_type == "GAMEPAD":
		$Buttons/NinePatchRect/MarginContainer/Credits/BackButton.grab_focus();

func _input(event):
	if event is InputEventKey:
		controller_type = "KEYBOARD";
	elif event is InputEventJoypadButton || event is InputEventJoypadMotion:
		if controller_type != "GAMEPAD":
			if $Buttons/NinePatchRect/MarginContainer/Settings.visible:
				$Buttons/NinePatchRect/MarginContainer/Settings/BackButton.grab_focus();
			if $Buttons/NinePatchRect/MarginContainer/Credits.visible:
				$Buttons/NinePatchRect/MarginContainer/Credits/BackButton.grab_focus();
			else:
				$Buttons/NinePatchRect/MarginContainer/MainButtons/PlayButton.grab_focus();
		controller_type = "GAMEPAD";
