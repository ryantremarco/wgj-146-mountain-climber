extends KinematicBody2D

const UP = Vector2(0, -1);

export var SPEED = 50;
export var GRAVITY = 1000;

var motion = Vector2();
var x_dir = -1;

var landed = 0;

func set_direction(value):
	if value < 0:
		$AnimatedSprite.flip_h = false;
		x_dir = -1;
	else:
		$AnimatedSprite.flip_h = true;
		x_dir = 1;

func _process(delta):
	#Wrap around left and right of screen
	position.x = wrapf(position.x, 0, get_viewport_rect().size.x)
	
	#Sets some values for later like is_on_floor
	motion = move_and_slide(motion, UP);
	
	#Activate anything touched or stepped on if needed
	for i in get_slide_count():
		landed = 1;
		var collider = get_slide_collision(i).collider;
		#Is the collider next to the 
		if collider.name == "Cultist":
			collider.position.y = 256;
		if "Fireball" in collider.name:
			set_direction(-x_dir);
	
	motion.x = SPEED*x_dir*landed;
	motion.y += GRAVITY*delta;
