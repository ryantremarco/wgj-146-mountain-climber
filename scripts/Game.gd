extends Node2D

const half_ground = preload("res://scenes/statics/HalfGround.tscn");
const crumbling_half_ground = preload("res://scenes/statics/CrumblingHalfGround.tscn");
const fireball_obstacle = preload("res://scenes/entities/Fireball.tscn");

onready var START_HEIGHT = -$Cultist.position.y;
export var PLATFORM_INTERVAL = 32;

var controller_type = "KEYBOARD";

var rng = RandomNumberGenerator.new();

var score = 0;
var current_top_level = 0;

var game_over = false;

func _ready():
	#Get some random time-seeded values
	rng.randomize();

func _process(_delta):
	if Input.is_action_just_pressed("game_restart"):
		get_tree().reload_current_scene();
	if Input.is_action_just_pressed("game_exit"):
		get_tree().change_scene("res://scenes/MainMenu.tscn");
		
	if !game_over:
		#Gets us a workable height relative to the starting position
		var player_height = -$Cultist.position.y - START_HEIGHT;
		#Point per tile (16px tall)
		score = max(score, player_height / 16 );
		score = round(score)	
		$HUD.set_score(score);
		
		#Generate more level!
		while current_top_level < score + 10:
			generate_row(current_top_level);
			current_top_level += 1;
		
		#Remove blocks that have gone offscreen
		remove_offscreen_rows();
		
		#Did the player fall off?
		if $Cultist.position.y >= $Cultist/PlayerCamera.limit_bottom:
			game_over = true;
			$Music.stop();
			$Cultist.died();
			$HUD.game_over();
			pass

func generate_row(level):
	#Get a random number of tiles for the row
	var platform_count = rng.randi_range(1, 7);
	#Bogus value to match the first check in the while loop below since Godot
	#doesn't have a do while, and we don't wanna do the randomisation twice
	var positions_taken = [-1];
	
	#Generate platforms
	for _i in range(platform_count):
		var block_choice = rng.randi_range(1,4);
		var instance;
		
		#25% chance to get a crumbly boi
		if block_choice == 1:
			instance = crumbling_half_ground.instance();
		else:
			instance = half_ground.instance();
		
		#Place the tile somewhere there isn't one already
		var random_x_pos = -1;
		while positions_taken.has(random_x_pos):
			random_x_pos = rng.randi_range(0, 7);
		positions_taken.push_back(random_x_pos);
		
		instance.position = Vector2(random_x_pos * 16, -START_HEIGHT - level*PLATFORM_INTERVAL);
		$GroundBlocks.add_child(instance);

func remove_offscreen_rows():
	for block in $GroundBlocks.get_children():
		if block.position.y >= $Cultist/PlayerCamera.limit_bottom:
			block.queue_free();


func _on_ObstacleTimer_timeout():
	var obstacle = fireball_obstacle.instance();
	var blocks_to_land_on = [];
	
	var y_pos = $Cultist/PlayerCamera.limit_bottom - 144;
	for block in $GroundBlocks.get_children():
		if block.position.y < $Cultist.position.y:
			blocks_to_land_on.push_back(block);
	var x_pos = blocks_to_land_on[rng.randi_range(0, blocks_to_land_on.size() - 1)].position.x;
	
	obstacle.position = Vector2(x_pos, y_pos);
	obstacle.set_direction(rng.randi_range(-1, 0));
	$Obstacles.add_child(obstacle);

func _input(event):
	if event is InputEventKey:
		controller_type = "KEYBOARD";
	elif event is InputEventJoypadButton || event is InputEventJoypadMotion:
		controller_type = "GAMEPAD";
	
	$HUD.update_prompts(controller_type);
