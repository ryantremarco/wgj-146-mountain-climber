extends Camera2D

onready var initial_bottom_limit = limit_bottom;

func _process(_delta):
	#Stop camera from scrolling back down
	limit_bottom = min(limit_bottom, get_camera_position().y + initial_bottom_limit/2);
