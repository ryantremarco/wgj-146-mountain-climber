extends KinematicBody2D

const UP = Vector2(0, -1);

export var RUN_SPEED = 100;
export var JUMP_POWER = 300;
export var GRAVITY = 1000;

var motion = Vector2();

onready var sprites = [$AnimatedSprite, $LeftClone, $RightClone];

func _ready():
	#God it's distracting when they're playing in the editor
	$AnimatedSprite.playing = true;

func died():
	$DeathSFX.play();

func _process(delta):	
	#Wrap around left and right of screen
	position.x = wrapf(position.x, 0, get_viewport_rect().size.x)
	
	#Sets some values for later like is_on_floor
	motion = move_and_slide(motion, UP);
	
	#Horizontal movement
	var h_speed = 0;
	if Input.is_action_pressed("player_right"):
		h_speed += RUN_SPEED;
	if Input.is_action_pressed("player_left"):
		h_speed -= RUN_SPEED;
	motion.x = h_speed;
	
	#Vertical movement
	motion.y += GRAVITY*delta;
	if is_on_floor():
		if Input.is_action_just_pressed("player_jump"):
			$JumpSFX.play();
			motion.y = -JUMP_POWER;
	
	#Honeeey, where's my super suit?
	update_sprite();
	
	#Activate anything touched or stepped on if needed
	for i in get_slide_count():
		var collider = get_slide_collision(i).collider;
		if collider.has_method("touched"):
			collider.touched();
		#Is the collider actually properly beneath the player?
		if position.y <= collider.position.y-$AnimatedSprite.frames.get_frame("idle", 0).get_size().y:
			if collider.has_method("stepped_on"):
				collider.stepped_on();

func update_sprite():
	#Don't forget the wraparound clones
	for sprite in sprites:
		if motion.x < 0:
			sprite.flip_h = true;
		elif motion.x > 0:
			sprite.flip_h = false;
		
		if is_on_floor():
			if motion.x == 0:
				sprite.play("idle");
			else:
				sprite.play("walk");
		else:	
			if motion.y < 0:
				sprite.play("jump");
			elif motion.y > 0:
				sprite.play("fall");
